.ONESHELL:
SHELL := /bin/bash
# GCP Project is obtained filtering gcloud projects
CLUSTER_NAME = "zonal-manager-cluster"
GCP_PROJECT := $(shell gcloud projects list | grep cluster-manager-app | cut -d ' ' -f1)
GCP_BASE_URL := gcr.io/$(GCP_PROJECT)
K8S_CTX := $(shell kubectl config current-context)
GCP_CLUSTER := $(shell gcloud container clusters list  | sed -n '1!p' | grep $(CLUSTER_NAME) | cut -d ' ' -f1)
PYTHON_DOCKER_IMG = cluster-manager
NGINX_DOCKER_IMG = nginx-cluster-manager
POSTGRES_DOCKER_IMG = postgres-cluster-manager
TAG = prod

# Deployment type (local for minikube or cloud)
ifneq ($(K8S_CTX), minikube)
define DEPLOYMENT_EXTRA_PARAMS
	--set python.image.repository=$(GCP_BASE_URL) \
	--set nginx.image.repository=$(GCP_BASE_URL) \
	--set postgres.image.repository=$(GCP_BASE_URL)
endef
endif

.PHONY: all
all: build deploy

.PHONY: print
print:
	@echo GCP_PROJECT IS $(GCP_PROJECT)
	@echo GCP_CLUSTER IS $(GCP_CLUSTER)
	@echo K8S_CTX IS $(K8S_CTX)
	@echo DEPLOYMENT_EXTRA_PARAMS IS $(DEPLOYMENT_EXTRA_PARAMS)

.PHONY: build
build:
	eval $(minikube docker-env)
	docker build -f docker/prod/postgres/Dockerfile -t $(POSTGRES_DOCKER_IMG):$(TAG) .
	docker build -f docker/prod/nginx/Dockerfile -t $(NGINX_DOCKER_IMG):$(TAG) .
	docker build -f docker/prod/python/Dockerfile -t $(PYTHON_DOCKER_IMG):$(TAG) .

.PHONY: tag
tag: build
	docker tag $(POSTGRES_DOCKER_IMG):$(TAG) $(GCP_BASE_URL)/$(POSTGRES_DOCKER_IMG):$(TAG)
	docker tag $(NGINX_DOCKER_IMG):$(TAG) $(GCP_BASE_URL)/$(NGINX_DOCKER_IMG):$(TAG)
	docker tag $(PYTHON_DOCKER_IMG):$(TAG) $(GCP_BASE_URL)/$(PYTHON_DOCKER_IMG):$(TAG)

.PHONY: push
push: tag
	docker push $(GCP_BASE_URL)/$(POSTGRES_DOCKER_IMG):$(TAG)
	docker push $(GCP_BASE_URL)/$(NGINX_DOCKER_IMG):$(TAG)
	docker push $(GCP_BASE_URL)/$(PYTHON_DOCKER_IMG):$(TAG)

.PHONY: deploy
deploy:
	kubectl create ns clustermgr || echo "Namespace already exists. Ignoring error."
	helm install $(DEPLOYMENT_EXTRA_PARAMS) --namespace clustermgr --timeout 200 --wait cluster-manager helm/cluster_manager/

.PHONY: clean
clean:
	gcloud container images delete $(GCP_BASE_URL)/$(POSTGRES_DOCKER_IMG):$(TAG) --force-delete-tags
	gcloud container images delete $(GCP_BASE_URL)/$(NGINX_DOCKER_IMG):$(TAG) --force-delete-tags
	gcloud container images delete $(GCP_BASE_URL)/$(PYTHON_DOCKER_IMG):$(TAG) --force-delete-tags
	helm del cluster-manager -n clustermgr
	kubectl delete secret db-passwords -n clustermgr

.PHONY: create-cluster
create-cluster:
	infra/dev-cluster.sh $(CLUSTER_NAME)

.PHONY: remove-cluster
remove-cluster:
ifneq "$(GCP_CLUSTER)" ""
	@echo Removing cluster $(GCP_CLUSTER)
	gcloud -q container clusters delete $(GCP_CLUSTER)
	kubectl config use-context minikube
else
	@echo No cluster to remove
endif
