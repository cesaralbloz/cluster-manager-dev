# TODO

- [ ] Deployment to automatically run migrations to enable Django admin app
- [x] Container port exposure for nginx and python
- [x] K8S services setup
- [x] Database container
- [x] Local minikube installation to avoid docker registry setup (until ready)
- [x] Persistent volume not mounted/storing data for postgres
- [x] Web style not being served by nginx/gunicorn
- [x] Verify GKE deployment (upload Docker images to registry)
- [ ] Staging vs prod deployment via helm values parametrization
- [x] Improve templatization for postgres
- [x] Create makefile to automate image generation and deployment
- [x] replicaSet differentiation for postgres and nginx+python deployment
- [x] Persistence for static content
- [ ] ~~Separate Nginx and Gunicorn into different Deployments~~
- [ ] ~~Expose 1 svc for Nginx and 1 for Gunicorn~~
- [ ] ~~Use gunicorn service to pass as input to the Nginx configuration (currently pointing to locahost)~~
