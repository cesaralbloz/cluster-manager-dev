server {
    listen 80;
    server_name localhost;
    access_log  /var/log/nginx/example.log;
    server_tokens off;

    location /static/ {
        autoindex off;
        alias /static_files/;
    }

    location / {
        try_files $uri $uri/ @python_django;
    }

    location @python_django {
		# Need to adjust for K8S/Docker deployment
		# The localhost parameter is only valid if Nginx is in the same POD as Gunicorn
		# If installed by using separate containers only, then the DNS resolution depends
		# on the name of the container and also the fact that they need to be in the same network
        proxy_pass http://localhost:8000;
        proxy_pass_request_headers on;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_redirect off;
    }
}
