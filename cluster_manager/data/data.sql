CREATE TABLE IF NOT EXISTS DATA_JSON (
    TEAM TEXT,
    ENVIRONMENT TEXT,
    CLOUD TEXT,
    K8S_NAMESPACE TEXT,
    ERIKUBE_CLIENT TEXT,
    PRIMARY KEY(TEAM, ENVIRONMENT)
);

INSERT INTO DATA_JSON (TEAM, ENVIRONMENT, CLOUD, K8S_NAMESPACE, ERIKUBE_CLIENT) VALUES ('adp', 'dev', 'eccd-ans-udm00144', 'udm-5gcicd', NULL),
   ('adp', 'ft', 'eccd-ans-udm00144', 'udm-5gcicd', 'armdocker.rnd.ericsson.se/proj_kds/erikube/client:0.4.4'),
   ('ausf', 'dev', 'hoff005', 'udm-5gcicd', NULL),
   ('ausf', 'ft', 'cram009', 'udm-5gcicd', 'armdocker.rnd.ericsson.se/proj_kds/erikube/client:0.4.4'),
   ('ccsm', 'dev', 'hoff005', 'udm-5gcicd', NULL),
   ('ccsm', 'ft', 'cram009', 'udm-5gcicd', 'armdocker.rnd.ericsson.se/proj_kds/erikube/client:0.4.4'),
   ('nef', 'dev', 'hoff005', 'udm-5gcicd', NULL),
   ('nef', 'ft', 'cram009', 'udm-5gcicd', 'armdocker.rnd.ericsson.se/proj_kds/erikube/client:0.4.4');
    