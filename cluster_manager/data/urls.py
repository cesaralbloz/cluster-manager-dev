from django.urls import path

from . import views

urlpatterns = [
    path('cluster/<str:team>/<str:environment>/', views.cluster, name='cluster'),
    path('cluster/get_cluster', views.get_cluster, name='get_cluster'),
    path('clustersql/<str:team>/<str:environment>/', views.cluster_sql, name='cluster_sql'),
    path('clustersql/get_cluster', views.get_cluster_sql, name='get_cluster_sql')
]
