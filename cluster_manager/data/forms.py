from django import forms

class EnvForm(forms.Form):
    team = forms.CharField(label='Team', max_length=100)
    environment = forms.CharField(label='Environment', max_length=100)