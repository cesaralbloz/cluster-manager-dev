import json
import psycopg2

from django.shortcuts import render
from django.http import JsonResponse

from .forms import EnvForm

with open('data/data.json') as f:
    data = json.load(f)

conn = psycopg2.connect(host='172.18.0.3', database='guestbook', user='django_user', password='password')
cur = conn.cursor()
sql = "select * from data_json where team=%s and environment=%s;"

def cluster(request, team='ADP', environment='dev'):
    try:
        result = data['ci'][team]['cluster'][environment]
    except KeyError:
        result = { 'error': 'No data for team %s and environment %s' % (team, environment)}
    return JsonResponse(result)

def get_cluster(request):
    if request.method == 'POST':
       form = EnvForm(request.POST)
       team = form.data['team']
       environment =  form.data['environment']
       return cluster(request, team=team, environment=environment)
    else:
        form = EnvForm()

    return render(request, 'data/env.html', {'form': form})

def cluster_sql(request, team='ADP', environment='dev'):
    result = cur.execute(sql, (team, environment))
    data = cur.fetchone()
    if data is None:
        #result = { 'error': 'No data for team %s and environment %s' % (team, environment)}
        cadena = sql % (team, environment)
        result = { 'cur': cur, 'query': str(cadena), 'data': data }
    else:
        result = {
            "cloud": data[2],
            "namespace": data[3],
            "erikube-client": data[4]
        }
    return JsonResponse(result)

def get_cluster_sql(request):
    if request.method == 'POST':
       form = EnvForm(request.POST)
       team = form.data['team']
       environment =  form.data['environment']
       return cluster_sql(request, team=team, environment=environment)
    else:
        form = EnvForm()

    return render(request, 'data/envsql.html', {'form': form})
