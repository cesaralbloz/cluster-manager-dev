# cluster-manager-dev

Cluster manager web app

### Building & Running Docker

```bash
# Build dev
docker build -f docker/dev/python/Dockerfile -t cluster-manager:dev .
# Build prod
docker build -f docker/prod/python/Dockerfile -t cluster-manager:prod .
# Build nginx
docker build -f docker/prod/nginx/Dockerfile -t nginx-cluster-manager:prod .

# Run dev
docker run --rm -p 8000:8000 -v $PWD/cluster_manager:/app/ -d cluster-manager:dev
# Run prod
docker run --rm -p 8000:8000 -v $PWD/cluster_manager:/app/ -d cluster-manager:prod
# Run nginx
docker run --rm -p 8080:80 -d nginx-cluster-manager:prod
```

> In order to run NGINX + PROD container, static files will be handled by NGINX

```bash
# Create volume 'static_files'
docker volume create --name static_files

# Create a network to allow hostname resolution
docker network create cluster-manager-nw

# Run Gunicorn server
docker run --rm -d \
--network=cluster-manager-nw \
-p 8000:8000 \
-v $PWD/cluster_manager:/app \
-v static_files:/static_files \
--name cluster-manager \
cluster-manager:prod

# Run Nginx server
docker run --rm -d \
--network=cluster-manager-nw \
-p 8080:80 \
-v static_files:/static_files \
--name nginx \
nginx-cluster-manager:prod
```

##  Working with Minikube

To build images so that these are available in Minikube without needing to use
a Docker registry, the following command shall be executed to configure the
shell environment

```bash
eval $(minikube docker-env)
```
In order to deploy the Helm chart to Minikube

```bash
# 1. Create a namespace in K8S (Minikube)
kubectl create ns demo
# 2. Deploying to the namespace naming the release 'demo-app'
helm install demo-app helm/cluster_manager --namespace demo
# 3. Proxy the POD running both Nginx and Gunicorn to access using the service
export POD_NAME=$(kubectl get pods --namespace demo \
	-l "app.kubernetes.io/name=cluster-manager,app.kubernetes.io/instance=demo-app" \
	-o jsonpath="{.items[0].metadata.name}")
kubectl --namespace demo port-forward $POD_NAME 8080:80
# 4. Navigate to http://localhost:8080 and acces Django demo page
```
